package io.healthforge;

import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LabResultsCsvParserTests {

    @Test
    public void testParsingLabResultsToJson() throws IOException {
        File patientsJsonFile = new File("./data/patients.json");
        //File labResultsFile = new File("./data/labresults-small.csv");
        File labResultsFile = new File("./data/labresults.csv");
        File labResultsCodesFile = new File("./data/labresults-codes.csv");

        File testFile = new File("./test.json");

        FileWriter out = new FileWriter(testFile, false);
        String result = LabResultsCsvParser.parseLabResultsCsvToJson(patientsJsonFile, labResultsFile, labResultsCodesFile);
        out.write(result);
        out.flush();
    }
}
