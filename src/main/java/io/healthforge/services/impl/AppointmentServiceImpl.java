package io.healthforge.services.impl;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import io.healthforge.models.Appointment;
import io.healthforge.services.AppointmentsService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * AppointmentServiceImpl service implementation
 */
@Component
public class AppointmentServiceImpl extends BaseServiceImpl<Appointment> implements AppointmentsService {

    @Override
    protected List<Appointment> getDoFilter(List<Appointment> items, Map<String, Object> searchParams) {
        if(searchParams.size() > 0) {
            if(searchParams.containsKey("patientId")) {
                UUID patientId = (UUID) searchParams.get("patientId");
                items = Lists.newArrayList(Collections2.filter(items, new Predicate<Appointment>() {
                    @Override
                    public boolean apply(Appointment appointment) {
                        if(appointment.getPatientId() != null && appointment.getPatientId().equals(patientId)) {
                            return true;
                        }
                        return false;
                    }
                }));
            }
        }
        return items;
    }
}
