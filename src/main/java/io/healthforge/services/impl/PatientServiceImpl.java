/*
 * Copyright (c) 2016. All rights reserved, HealthForge.
 */

package io.healthforge.services.impl;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import io.healthforge.models.Address;
import io.healthforge.models.Patient;
import io.healthforge.services.PatientService;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Patient service implementation
 */
@Component
public class PatientServiceImpl extends BaseServiceImpl<Patient> implements PatientService {
    @Override
    protected List<Patient> getDoFilter(List<Patient> items, Map<String, Object> searchParams) {
        if(searchParams.size() > 0) {
            if(searchParams.containsKey("dob")) {
                LocalDate dob = ((DateTime)searchParams.get("dob")).toLocalDate();
                items = Lists.newArrayList(Collections2.filter(items, new Predicate<Patient>() {
                    @Override
                    public boolean apply(Patient patient) {
                        if(patient.getDateOfBirth() != null && patient.getDateOfBirth().toLocalDate().isEqual(dob)) {
                            return true;
                        }
                        return false;
                    }
                }));
            }

            if(searchParams.containsKey("postcode")) {
                String postcode = (String) searchParams.get("postcode");
                items = Lists.newArrayList(Collections2.filter(items, new Predicate<Patient>() {
                    @Override
                    public boolean apply(Patient patient) {
                        List<Address> matchingAddresses =
                            patient.getAddresses().stream().filter(
                                (address) -> addressContainsPostcode(address, postcode)
                            ).collect(Collectors.toList());
                        return !matchingAddresses.isEmpty();
                    }
                }));
            }
        }
        return items;
    }

    private boolean addressContainsPostcode(Address address, String postcode) {
        String addressPostcode = getPostcode(address);
        return addressPostcode != null && addressPostcode.equals(postcode);
    }

    private String getPostcode(Address address) {
        String country = address.getCountry();
        switch(country) {
            case "Argentina" :
                return address.getLines().get(2).split(" ")[0];
            case "People's Republic of China" :
                return address.getLines().get(5);
            case "Belarus" :
            case "Беларусь" :
                return address.getLines().get(2).split(",")[0];
            case "Japan" :
                return address.getLines().get(0).split("〒")[1];
            case "USA" :
                return address.getLines().get(2).split(" ")[1];
            default :
                return null;
        }
    }
}
