package io.healthforge.services;

import io.healthforge.models.Appointment;

/**
 * A CRUD service for managing appointments.
 */
public interface AppointmentsService extends BaseService<Appointment> {

}
