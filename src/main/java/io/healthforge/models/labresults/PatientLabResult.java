package io.healthforge.models.labresults;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PatientLabResult {

    private UUID id;
    private String firstName;
    private String lastName;
    private DateTime dob;
    private List<LabResult> lab_results;

    public PatientLabResult(UUID id, String firstName, String lastName, DateTime dob) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.lab_results = new ArrayList<LabResult>();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public DateTime getDob() {
        return dob;
    }

    public void setDob(DateTime dob) {
        this.dob = dob;
    }

    public List<LabResult> getLab_results() {
        return lab_results;
    }

    public void setLab_results(List<LabResult> lab_results) {
        this.lab_results = lab_results;
    }
}

