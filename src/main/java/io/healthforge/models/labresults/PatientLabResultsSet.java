package io.healthforge.models.labresults;

import java.util.Collection;

public class PatientLabResultsSet {

    private Collection<PatientLabResult> patients;

    public Collection<PatientLabResult> getPatients() {
        return patients;
    }

    public void setPatients(Collection<PatientLabResult> patients) {
        this.patients = patients;
    }

}
