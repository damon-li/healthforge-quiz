package io.healthforge.models.labresults;

public class LabResultTest {

    private String code;
    private String label;
    private Double value;
    private String unit;
    private Double lower;
    private Double upper;

    public LabResultTest(String code, String label, Double value, String unit, Double lower, Double upper) {
        this.code = code;
        this.label = label;
        this.value = value;
        this.unit = unit;
        this.lower = lower;
        this.upper = upper;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getLower() {
        return lower;
    }

    public void setLower(Double lower) {
        this.lower = lower;
    }

    public Double getUpper() {
        return upper;
    }

    public void setUpper(Double upper) {
        this.upper = upper;
    }
}
