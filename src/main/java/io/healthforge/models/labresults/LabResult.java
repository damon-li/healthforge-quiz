package io.healthforge.models.labresults;

import org.joda.time.DateTime;

import java.util.List;

public class LabResult {
    private DateTime timestamp;
    private LabResultProfile labResultProfile;
    private List<LabResultTest> panel;

    public LabResult(DateTime timestamp, LabResultProfile labResultProfile, List<LabResultTest> panel) {
        this.timestamp = timestamp;
        this.labResultProfile = labResultProfile;
        this.panel = panel;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }

    public LabResultProfile getLabResultProfile() {
        return labResultProfile;
    }

    public void setLabResultProfile(LabResultProfile labResultProfile) {
        this.labResultProfile = labResultProfile;
    }

    public List<LabResultTest> getPanel() {
        return panel;
    }

    public void setPanel(List<LabResultTest> panel) {
        this.panel = panel;
    }
}
