package io.healthforge.models.labresults;

public class LabResultCode {
    private String key;
    private String code;
    private String description;

    public LabResultCode(String key, String code, String description) {
        this.key = key;
        this.code = code;
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
