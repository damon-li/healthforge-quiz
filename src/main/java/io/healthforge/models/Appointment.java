package io.healthforge.models;

import org.joda.time.DateTime;
import java.util.UUID;

public class Appointment extends BaseEntity {

    private UUID id;
    private Status status;

    private DateTime startsAt;

    private DateTime endAt;
    private UUID clinicianId;
    private String reason;
    private UUID patientId;
    public Appointment() {}

    public Appointment(UUID id, Status status, DateTime startsAt, DateTime endAt, UUID patientId, UUID clinicianId, String reason) {
        this.id = id;
        this.status = status;
        this.startsAt = startsAt;
        this.endAt = endAt;
        this.patientId = patientId;
        this.clinicianId = clinicianId;
        this.reason = reason;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public DateTime getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(DateTime startsAt) {
        this.startsAt = startsAt;
    }

    public DateTime getEndAt() {
        return endAt;
    }

    public void setEndAt(DateTime endAt) {
        this.endAt = endAt;
    }

    public UUID getClinicianId() {
        return clinicianId;
    }

    public void setClinicianId(UUID clinicianId) {
        this.clinicianId = clinicianId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }
}
