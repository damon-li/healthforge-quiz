package io.healthforge.models;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Status {

    NEW("NEW"),
    PENDING("PENDING"),
    IN_PROGRESS("IN_PROGRESS"),
    COMPLETE("COMPLETE"),
    CANCEL("CANCEL"),
    CANCELLED("CANCELLED"),
    DNA("DNA");

    private String jsonValue;

    private Status(final String jsonValue) {
        this.jsonValue = jsonValue;
    }

    @JsonValue
    public String jsonValue() {
        return this.jsonValue;
    }
}
