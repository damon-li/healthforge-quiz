package io.healthforge.rest;

import io.healthforge.models.Appointment;
import io.healthforge.models.GenericResponse;
import io.healthforge.models.ResultSet;
import io.healthforge.services.AppointmentsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jodd.util.MimeTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * CRUD REST API for handling appointments
 */
@RestController
@Api(value = "appointments", description = "Appointment API")
@RequestMapping("/api/appointments")
public class AppointmentsApi {

    private final static Logger logger = LoggerFactory.getLogger(AppointmentsApi.class);

    @Autowired
    private AppointmentsService service;

    @RequestMapping(value = "", method = RequestMethod.GET,
            produces = {MimeTypes.MIME_APPLICATION_JSON})
    @ApiOperation(tags = { "appointments" }, value = "Gets the appointments.")
    public ResultSet<Appointment> getAll(
            @RequestParam(name="offset", defaultValue="0", required=false) Integer offset,
            @RequestParam(name="limit", defaultValue="10", required=false) Integer limit,
            @RequestParam(name="patientId", required=false) String patientId) {

        Map<String, Object> searchParams = new HashMap<>();
        if(StringUtils.hasText(patientId)) {
            searchParams.put("patientId", UUID.fromString(patientId));
        }

        return service.get(offset, limit, searchParams);
    }

    @RequestMapping(value = "/{appointmentId}", method = RequestMethod.GET,
            produces = {MimeTypes.MIME_APPLICATION_JSON})
    @ApiOperation(tags = { "appointments" }, value = "Get by id.", notes = "Returns an appointment by id.")
    public Appointment get(@PathVariable String appointmentId) throws NotFoundException {
        return service.get(UUID.fromString(appointmentId));
    }

    @RequestMapping(value = "", method = RequestMethod.POST,
            consumes = {MimeTypes.MIME_APPLICATION_JSON},
            produces = {MimeTypes.MIME_APPLICATION_JSON})
    @ApiOperation(tags = { "appointments" }, value = "Creates an appointment.")
    public Appointment post(@RequestBody Appointment appointment) throws NotFoundException {
        return service.add(appointment);
    }

    @RequestMapping(value = "/{appointmentId}", method = RequestMethod.PUT,
            consumes = {MimeTypes.MIME_APPLICATION_JSON},
            produces = {MimeTypes.MIME_APPLICATION_JSON})
    @ApiOperation(tags = { "appointments" }, value = "Updates an appointment.",
            notes = "Updates an appointment.")
    public Appointment put(@PathVariable String appointmentId, @RequestBody Appointment appointment) throws NotFoundException {
        if(!appointment.getId().equals(UUID.fromString(appointmentId))) {
            throw new NotFoundException();
        }
        return service.update(appointment);
    }

    @RequestMapping(value = "/{appointmentId}", method = RequestMethod.DELETE,
            produces = {MimeTypes.MIME_APPLICATION_JSON})
    @ApiOperation(tags = { "appointments" }, value = "Deletes an appointment.", notes = "")
    public GenericResponse delete(@PathVariable String appointmentId)throws NotFoundException {
        service.remove(UUID.fromString(appointmentId));
        return GenericResponse.OK();
    }
}
