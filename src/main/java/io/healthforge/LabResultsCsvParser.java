package io.healthforge;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import io.healthforge.models.Patient;
import io.healthforge.models.labresults.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.*;

public class LabResultsCsvParser {

    public static String parseLabResultsCsvToJson(File patientsJson, File labResultsCsvFile, File labResultsCodesFile) throws IOException {

        // Generate the map from hospital id and lab results by parsing the lab results and lab results codes csv files
        Map<String, List<LabResult>> hospIdToResultsMap = parseLabResultsCsvToLabResultsMap(labResultsCsvFile, labResultsCodesFile);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JodaModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        // Generate the map from patient identifiers (linked to the hospital ids from earlier) to the patient lab result object containing required patient data
        Map<String, PatientLabResult> identifiersToPatientResults = new HashMap<String, PatientLabResult>();
        List<Patient> patients = objectMapper.readValue(patientsJson, new TypeReference<List<Patient>>(){});

        for (Patient patient : patients) {
            PatientLabResult patientLabResult = new PatientLabResult(patient.getId(), patient.getFirstName(), patient.getLastName(), patient.getDateOfBirth());
            for (String identifier : patient.getIdentifiers()) {
                identifiersToPatientResults.put(identifier, patientLabResult);
            }
        }

        // Generate the patient lab results object by combining the lab results and patient data linked with lab result hospital id/patient identifiers
        PatientLabResultsSet resultsSet = new PatientLabResultsSet();

        for (String hospId : hospIdToResultsMap.keySet()) {
            if (identifiersToPatientResults.containsKey(hospId)) {
                PatientLabResult patientResults = identifiersToPatientResults.get(hospId);
                patientResults.getLab_results().addAll(hospIdToResultsMap.get(hospId));
            }
        }
        resultsSet.setPatients(identifiersToPatientResults.values());

        // Serialize the patient lab results object into json
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        objectMapper.writeValue(outputStream, resultsSet);
        return new String(outputStream.toByteArray());
    }

    private static Map<String, String> parseLabResultsCodes(File labResultsCodesFile) throws IOException {
        Map<String, String> keyToCodeMap = new HashMap<String, String>();

        CsvMapper csvMapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        MappingIterator<Map<String, String>> iterator = csvMapper.readerFor(Map.class).with(schema).readValues(labResultsCodesFile);

        while(iterator.hasNext()) {
            Map<String, String> row = iterator.next();
            String key = row.get("key");
            String code = row.get("code");

            if (StringUtils.hasText(code))
                keyToCodeMap.put(key, code);
        }

        return keyToCodeMap;
    }

    private static Map<String, List<LabResult>> parseLabResultsCsvToLabResultsMap(File labResultsCsvFile, File labResultsCodesFile) throws IOException {
        Map<String, List<LabResult>> hospIdToLabResultsMap = new HashMap<String, List<LabResult>>();

        Map<String, String> labResultKeyToCodeMap = parseLabResultsCodes(labResultsCodesFile);

        Map<String, Set<String>> hospIdToSampleIdMap = new HashMap<String, Set<String>>();

        Map<String, LabResult> sampleIdToResultsMap = new HashMap<String, LabResult>();

        CsvMapper mapper = new CsvMapper();
        mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
        MappingIterator<String[]> iterator = mapper.readerFor(String[].class).readValues(new FileInputStream(labResultsCsvFile));

        iterator.next(); // skip the first row as it's the headers

        while (iterator.hasNext()) {
            String[] data = iterator.next();

            String hospitalId = data[0]; // HospID

            if (!hospIdToSampleIdMap.containsKey(hospitalId)) {
                hospIdToSampleIdMap.put(hospitalId, new HashSet<String>());
            }

            String sampleId = data[1]; // SampleID

            hospIdToSampleIdMap.get(hospitalId).add(sampleId);

            if (!sampleIdToResultsMap.containsKey(sampleId)) {
                DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
                DateTime date = DateTime.parse(data[2], formatter); // Date

                String profileName = data[3]; // Profile Name
                String profileCode = data[4]; // Profile Name

                LabResultProfile profile = new LabResultProfile(profileName, profileCode);
                LabResult labResult = new LabResult(date, profile, new ArrayList<LabResultTest>());
                sampleIdToResultsMap.put(sampleId, labResult);
            }

            String testName = data[30]; // TestName
            String unit = data[31]; // Unit
            Double lower = parseDouble(data[32]); // Lower
            Double upper = parseDouble(data[33]); // Upper

            for (int i=0; i < 25; i++) {
                String result = data[5 + i]; // result 1 is in column 5, result 2 is in column 6 etc.
                String[] codeAndValue = result.split("~");
                String code = codeAndValue[0];

                if (code.equals(testName)) {
                    // only add this is the testName equals the test code so we know this row is the row containing the details (units, lower etc.)
                    // for that particular result
                    Double value = parseDouble(codeAndValue[1]);
                    String snomedCode = labResultKeyToCodeMap.get(code);

                    LabResultTest test = new LabResultTest(snomedCode, code, value, unit, lower, upper);
                    sampleIdToResultsMap.get(sampleId).getPanel().add(test);
                    break;
                }
            }
        }

        for (String hospId : hospIdToSampleIdMap.keySet()) {
            List<LabResult> results = new ArrayList<LabResult>();
            Set<String> sampleIds = hospIdToSampleIdMap.get(hospId);
            for (String sampleId: sampleIds) {
                results.add(sampleIdToResultsMap.get(sampleId));
            }
            hospIdToLabResultsMap.put(hospId, results);
        }

        return hospIdToLabResultsMap;
    }

    private static Double parseDouble(String value) {
        Double doubleValue = null;
        if (StringUtils.hasText(value)) {
            if (value.equals(".")) {
                return 0.0;
            }
            else {
                return Double.parseDouble(value.replaceAll("[^\\d.]", ""));
            }
        }
        return doubleValue;
    }
}
