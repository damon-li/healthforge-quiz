# HealthForge Coding Quiz
by Damon Li

# Question 1

#### Should be completed after changes to classes:
- io/healthforge/services/impl/PatientServiceImpl.java
- io/healthforge/rest/PatientApi.java

    
# Question 2

Appointments service should now be available via http://localhost:8080/appointments (after running 'mvn spring-boot:run')

#### New classes created for appointments service:
- io/healthforge/models/Appointment.java
- io/healthforge/models/Status.java
- io/healthforge/rest/AppointmentsApi.java
- io/healthforge/services/AppointmentsService.java
- io/healthforge/services/impl/AppointmentServiceImpl.java


#### Initial example appointments json file created at:
src/main/resources/appointments.json


# Question 3

#### New class created for parsing lab results csv:
io/healthforge/LabResultsCsvParser


#### New Lab results model classes created in package:
io/healthforge/models/labresults


#### Example test created at:
src/test/java/io/healthforge/LabResultsCsvParserTests.java
which generates test.json file at ./test.json

#### Smaller labresults files created for testing at:
data/labresults-small.csv